package com.morozov.tm.util;


import com.morozov.tm.endpoint.Project;
import com.morozov.tm.endpoint.Task;
import com.morozov.tm.enumerated.CompareTypeUnum;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.xml.datatype.XMLGregorianCalendar;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

public class ConsoleHelperUtil {

    @NotNull
    public static final String DATA_FORMAT = "dd.MM.yyyy";

    @NotNull
    public static String readString() {
        @NotNull String enterString = "";
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        try {
            enterString = bufferedReader.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return enterString;
    }

    public static void writeString(@NotNull final String string) {
        System.out.println(string);
    }

    public static Date formattedData(@NotNull final String data) throws ParseException {
        if (data.isEmpty()) return null;
        @NotNull final SimpleDateFormat format = new SimpleDateFormat(DATA_FORMAT);
        @Nullable final Date resultDate = format.parse(data);
        return resultDate;
    }

    public static void printProjectList(@NotNull final List<Project> projectList) {
        int indexList = 1;
        for (final Project project : projectList) {
            final String projectName = project.getName();
            final String dataCreate = formattedDataToString(project.getCreatedData());
            final String dataStart = formattedDataToString(project.getStartDate());
            final String dataEnd = formattedDataToString(project.getEndDate());
            final String currentStatus = project.getStatus().value();
            final String id = project.getId();
            ConsoleHelperUtil.writeString(String.format("%d: Имя : %s | Дата создания: %s |" +
                            "| дата начала: %s | дата завершения: %s | Статус: %s | ID: %s",
                    indexList, projectName, dataCreate, dataStart, dataEnd, currentStatus, id));
            indexList++;
        }
    }
    public static void printTaskList(@NotNull final List<Task> projectList) {
        int indexList = 1;
        for (final Task task : projectList) {
            final String projectName = task.getName();
            final String dataCreate = formattedDataToString(task.getCreatedData());
            final String dataStart = formattedDataToString(task.getStartDate());
            final String dataEnd = formattedDataToString(task.getEndDate());
            final String currentStatus = task.getStatus().value();
            final String id = task.getId();
            ConsoleHelperUtil.writeString(String.format("%d: Имя : %s | Дата создания: %s |" +
                            "| дата начала: %s | дата завершения: %s | Статус: %s | ID: %s",
                    indexList, projectName, dataCreate, dataStart, dataEnd, currentStatus, id));
            indexList++;
        }
    }

    public static String formattedDataToString(XMLGregorianCalendar date) {
        if(date == null) return "не установлена";
        return date.toString();
    }

    public static CompareTypeUnum printSortedVariant() {
        ConsoleHelperUtil.writeString("Варианты сортировки: ");
        final HashMap<String, CompareTypeUnum> compareTypeEnumHashMap = new HashMap<>();
        for (CompareTypeUnum compareTypeUnum : CompareTypeUnum.values()) {
            String enumName = compareTypeUnum.getName();
            System.out.println(enumName + " : " + compareTypeUnum.getDescription());
            compareTypeEnumHashMap.put(enumName, compareTypeUnum);
        }
        final String type = ConsoleHelperUtil.readString();
        return compareTypeEnumHashMap.get(type);
    }
}
