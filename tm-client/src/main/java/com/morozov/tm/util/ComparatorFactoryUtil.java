package com.morozov.tm.util;

import com.morozov.tm.endpoint.AbstractWorkEntity;
import com.morozov.tm.enumerated.CompareTypeUnum;
import com.morozov.tm.util.comparator.DataCreateComparator;
import com.morozov.tm.util.comparator.DataEndComparator;
import com.morozov.tm.util.comparator.DataStartComparator;
import com.morozov.tm.util.comparator.StatusComparator;
import org.jetbrains.annotations.NotNull;

import java.util.Comparator;

public class ComparatorFactoryUtil {
    public static Comparator<AbstractWorkEntity> getComparator(@NotNull final CompareTypeUnum compareTypeUnum){
        Comparator<AbstractWorkEntity> comparator = null;
        switch (compareTypeUnum){
            case DATACREATE:
                comparator = new DataCreateComparator();
                break;
            case DATASTART:
                comparator = new DataStartComparator();
                break;
            case DATAEND:
                comparator = new DataEndComparator();
                break;
            case STATUS:
                comparator = new StatusComparator();
        }
        return comparator;
    }
}
