package com.morozov.tm.exception;

public class RepositoryEmptyException extends Exception {
    public RepositoryEmptyException() {
        super("Список пуст");
    }
}
