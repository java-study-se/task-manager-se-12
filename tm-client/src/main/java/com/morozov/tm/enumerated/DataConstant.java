package com.morozov.tm.enumerated;

import java.io.File;

public enum DataConstant {
    DATA_FILE_BIN("." + File.separator + "data" + File.separator + "data.bin"),
    DATA_FILE_JSON("." + File.separator + "data" + File.separator + "data.json"),
    DATA_FILE_XML("." + File.separator + "data" + File.separator + "data.xml");
    private String path;

    DataConstant(String path) {
        this.path = path;
    }

    public String getPath() {
        return path;
    }
}
