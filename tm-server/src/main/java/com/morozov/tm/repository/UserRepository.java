package com.morozov.tm.repository;

import com.morozov.tm.api.IUserRepository;
import com.morozov.tm.entity.User;
import com.morozov.tm.enumerated.FieldConst;
import com.morozov.tm.enumerated.UserRoleEnum;
import com.morozov.tm.exception.ConnectionLostException;
import com.morozov.tm.util.MD5HashUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public final class UserRepository extends AbstractRepository implements IUserRepository<User> {

    public UserRepository(@Nullable Connection connection) {
        super(connection);
    }

    @Nullable
    @Override
    public User findOneByLogin(@NotNull final String login) throws ConnectionLostException, SQLException {
        @NotNull final List<User> userList = findAll();
        User foundUser = null;
        for (@NotNull final User user : userList) {
            if (user.getLogin().equals(login)) foundUser = user;
        }
        return foundUser;
    }

    @Override
    public @NotNull List<User> findAll() throws ConnectionLostException, SQLException {
        @NotNull final String query = "SELECT * FROM tm_morozov.app_user;";
        if (getConnection() == null) throw new ConnectionLostException();
        @NotNull final PreparedStatement statement = getConnection().prepareStatement(query);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        @NotNull final List<User> resultList = new ArrayList<>();
        while (resultSet.next()) resultList.add(fetch(resultSet));
        statement.close();
        return resultList;
    }

    @Nullable
    @Override
    public User findOne(@NotNull String id) throws SQLException, ConnectionLostException {
        @NotNull final String query = "SELECT * FROM tm_morozov.app_user WHERE id = ?;";
        if (getConnection() == null) throw new ConnectionLostException();
        @NotNull final PreparedStatement statement = getConnection().prepareStatement(query);
        statement.setString(1, id);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        //@Nullable User user = fetch(resultSet);
        @Nullable User user = new User();
        while(resultSet.next()) user = fetch(resultSet);
        return user;
    }

    @Override
    public void persist(@NotNull final User writeEntity) throws ConnectionLostException, SQLException {
        @NotNull final String query = "INSERT INTO tm_morozov.app_user (id, login, password_hash, role) VALUES (?,?,?,?);";
        if (getConnection() == null) throw new ConnectionLostException();
        @NotNull final PreparedStatement statement = getConnection().prepareStatement(query);
        statement.setString(1, writeEntity.getId());
        statement.setString(2, writeEntity.getLogin());
        statement.setString(3, writeEntity.getPasswordHash());
        statement.setString(4, writeEntity.getRole().name());
        statement.execute();
    }

    @Override
    public void merge(@NotNull final User updateEntity) throws ConnectionLostException, SQLException {
        if (findOne(updateEntity.getId()) == null) {
            persist(updateEntity);
        } else {
            @NotNull final String query = "UPDATE tm_morozov.app_user SET " +
                    "login = ? , password_hash = ? , role = ? " +
                    "WHERE  id = ?;";
            if (getConnection() == null) throw new ConnectionLostException();
            @NotNull final PreparedStatement statement = getConnection().prepareStatement(query);
            statement.setString(1,updateEntity.getLogin());
            statement.setString(2,updateEntity.getPasswordHash());
            statement.setString(3,updateEntity.getRole().name());
            statement.setString(4,updateEntity.getId());
        }
    }


    @Override
    public void remove(@NotNull final String id) throws ConnectionLostException, SQLException {
        @NotNull final String query = "DELETE FROM tm_morozov.app_user WHERE id=?;";
        if (getConnection() == null) throw new ConnectionLostException();
        @NotNull final PreparedStatement statement = getConnection().prepareStatement(query);
        statement.setString(1, id);
        statement.execute();
    }

    @Override
    public void removeAll() throws ConnectionLostException, SQLException {
        @NotNull final String query = "DELETE FROM tm_morozov.app_user;";
        if (getConnection() == null) throw new ConnectionLostException();
        @NotNull final PreparedStatement statement = getConnection().prepareStatement(query);
        statement.execute();
    }

    @Override
    public void load(@NotNull final List<User> loadEntityList) throws ConnectionLostException, SQLException {
        for (@NotNull final User user: loadEntityList) {
            merge(user);
        }
    }

    @Nullable
    private User fetch(@Nullable final ResultSet row) throws SQLException {
        if (row == null) return null;
        @NotNull final User user = new User();
        user.setId(row.getString(FieldConst.ID.name()));
        user.setLogin(row.getString(FieldConst.LOGIN.name()));
        user.setPasswordHash(row.getString(FieldConst.PASSWORD_HASH.name()));
        user.setRole(UserRoleEnum.valueOf(row.getString(FieldConst.ROLE.name().toLowerCase())));
        return user;
    }
}



