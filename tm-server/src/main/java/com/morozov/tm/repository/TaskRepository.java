package com.morozov.tm.repository;

import com.morozov.tm.api.ITaskRepository;
import com.morozov.tm.entity.Project;
import com.morozov.tm.entity.Task;
import com.morozov.tm.enumerated.FieldConst;
import com.morozov.tm.enumerated.StatusEnum;
import com.morozov.tm.exception.ConnectionLostException;
import com.morozov.tm.util.FormatConvertUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public final class TaskRepository extends AbstractRepository implements ITaskRepository<Task> {
    public TaskRepository(@Nullable Connection connection) {
        super(connection);
    }

    @NotNull
    @Override
    public List<Task> findAllByProjectIdUserId(@NotNull final String userId, @NotNull final String projectId) throws ConnectionLostException, SQLException {
        @NotNull final List<Task> resultListByUserId = new ArrayList<>();
        for (@NotNull final Task task : getAllTaskByProjectId(projectId)) {
            if (task.getUserId().equals(userId)) resultListByUserId.add(task);
        }
        return resultListByUserId;
    }

    @NotNull
    @Override
    public List<Task> findAllByUserId(@NotNull final String userId) throws ConnectionLostException, SQLException {
        @NotNull final String query = "SELECT * FROM tm_morozov.app_task WHERE user_id = ?;";
        if (getConnection() == null) throw new ConnectionLostException();
        @NotNull final PreparedStatement statement = getConnection().prepareStatement(query);
        statement.setString(1, userId);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        @NotNull final List<Task> resultList = new ArrayList<>();
        while (resultSet.next()) resultList.add(fetch(resultSet));
        statement.close();
        return resultList;
    }

    @Nullable
    @Override
    public Task findOneByUserId(@NotNull final String userId, @NotNull final String id) throws ConnectionLostException, SQLException {
        Task resultTask = null;
        for (@NotNull final Task task : findAllByUserId(userId)) {
            if (task.getId().equals(id)) resultTask = task;
        }
        return resultTask;
    }

    @NotNull
    @Override
    public List<Task> getAllTaskByProjectId(@NotNull final String projectId) throws ConnectionLostException, SQLException {
        @NotNull List<Task> resultList = new ArrayList<>();
        for (@NotNull final Task task : findAll()) {
            if (task.getIdProject().equals(projectId)) resultList.add(task);
        }
        return resultList;
    }

    @Override
    public void deleteAllTaskByProjectId(@NotNull final String userId, @NotNull final String projectId) throws ConnectionLostException, SQLException {
        @NotNull final List<Task> taskToDelete = findAllByProjectIdUserId(userId, projectId);
        for (@NotNull final Task task : taskToDelete) {
            remove(task.getId());
        }
    }

    @Override
    public List<Task> findTaskByStringInNameOrDescription(@NotNull final String userId, @NotNull final String string) throws ConnectionLostException, SQLException {
        @NotNull final String query = "SELECT * FROM tm_morozov.app_task WHERE user_id = '" + userId + "' AND " +
                "(name LIKE '%" + string + "%' OR description LIKE '%" + string + "%');";
        if (getConnection() == null) throw new ConnectionLostException();
        @NotNull final PreparedStatement statement = getConnection().prepareStatement(query);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        @NotNull final List<Task> resultList = new ArrayList<>();
        while (resultSet.next()) resultList.add(fetch(resultSet));
        statement.close();
        return resultList;
    }

    @Override
    public void removeAllByUserId(@NotNull final String userId) throws ConnectionLostException, SQLException {
        @NotNull final String query = "DELETE FROM tm_morozov.app_task WHERE user_id = ?;";
        if (getConnection() == null) throw new ConnectionLostException();
        @NotNull final PreparedStatement statement = getConnection().prepareStatement(query);
        statement.setString(1, userId);
        statement.execute();
    }


    @Override
    public @NotNull List<Task> findAll() throws ConnectionLostException, SQLException {
        @NotNull final String query = "SELECT * FROM tm_morozov.app_task;";
        if (getConnection() == null) throw new ConnectionLostException();
        @NotNull final PreparedStatement statement = getConnection().prepareStatement(query);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        @NotNull final List<Task> resultList = new ArrayList<>();
        while (resultSet.next()) resultList.add(fetch(resultSet));
        statement.close();
        return resultList;
    }

    @Nullable
    @Override
    public Task findOne(@NotNull String id) throws SQLException, ConnectionLostException {
        @NotNull final String query = "SELECT * FROM tm_morozov.app_task WHERE id = ?;";
        if (getConnection() == null) throw new ConnectionLostException();
        @NotNull final PreparedStatement statement = getConnection().prepareStatement(query);
        statement.setString(1, id);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        @Nullable Task task = null;
        while (resultSet.next()) task = fetch(resultSet);
        return task;
    }

    @Override
    public void persist(@NotNull Task writeEntity) throws ConnectionLostException, SQLException {
        @NotNull final String query = "INSERT INTO tm_morozov.app_task (id, date_create, date_end, date_start, description," +
                " name, status, user_id, project_id) VALUES (?,?,?,?,?,?,?,?,?);";
        if (getConnection() == null) throw new ConnectionLostException();
        @NotNull final PreparedStatement statement = getConnection().prepareStatement(query);
        statement.setString(1, writeEntity.getId());
        statement.setDate(2, FormatConvertUtil.convertDateToSqlDate(writeEntity.getCreatedData()));
        statement.setDate(3, FormatConvertUtil.convertDateToSqlDate(writeEntity.getEndDate()));
        statement.setDate(4, FormatConvertUtil.convertDateToSqlDate(writeEntity.getStartDate()));
        statement.setString(5, writeEntity.getDescription());
        statement.setString(6, writeEntity.getName());
        statement.setString(7, writeEntity.getStatus().name());
        statement.setString(8, writeEntity.getUserId());
        statement.setString(9, writeEntity.getIdProject());
        statement.execute();
    }

    @Override
    public void merge(@NotNull Task updateEntity) throws ConnectionLostException, SQLException {
        if (findOne(updateEntity.getId()) == null) {
            persist(updateEntity);
        } else {
            @NotNull final String query = "UPDATE tm_morozov.app_task SET " +
                    "date_create = ? , date_end = ? , date_start = ? , description = ? , " +
                    "name = ? , status = ? , user_id = ? , project_id = ? WHERE  id = ?;";
            if (getConnection() == null) throw new ConnectionLostException();
            @NotNull final PreparedStatement statement = getConnection().prepareStatement(query);
            statement.setDate(1, FormatConvertUtil.convertDateToSqlDate(updateEntity.getCreatedData()));
            statement.setDate(2, FormatConvertUtil.convertDateToSqlDate(updateEntity.getEndDate()));
            statement.setDate(3, FormatConvertUtil.convertDateToSqlDate(updateEntity.getStartDate()));
            statement.setString(4, updateEntity.getDescription());
            statement.setString(5, updateEntity.getName());
            statement.setString(6, updateEntity.getStatus().name());
            statement.setString(7, updateEntity.getUserId());
            statement.setString(8, updateEntity.getIdProject());
            statement.setString(9, updateEntity.getId());
            statement.execute();
        }
    }

    @Override
    public void remove(@NotNull String id) throws ConnectionLostException, SQLException {
        @NotNull final String query = "DELETE FROM tm_morozov.app_task WHERE id = ?;";
        if (getConnection() == null) throw new ConnectionLostException();
        @NotNull final PreparedStatement statement = getConnection().prepareStatement(query);
        statement.setString(1, id);
        statement.execute();
    }

    @Override
    public void removeAll() throws ConnectionLostException, SQLException {
        @NotNull final String query = "DELETE FROM tm_morozov.app_task;";
        if (getConnection() == null) throw new ConnectionLostException();
        @NotNull final PreparedStatement statement = getConnection().prepareStatement(query);
        statement.execute();
    }

    @Override
    public void load(@NotNull List<Task> loadEntityList) throws ConnectionLostException, SQLException {
        for (@NotNull final Task task : loadEntityList) {
            merge(task);
        }
    }

    @Nullable
    private Task fetch(@Nullable final ResultSet row) throws SQLException {
        if (row == null) return null;
        @NotNull final Task task = new Task();
        task.setId(row.getString(FieldConst.ID.name()));
        task.setUserId(row.getString(FieldConst.USER_ID.name()));
        task.setIdProject(row.getString(FieldConst.PROJECT_ID.name()));
        task.setDescription(row.getString(FieldConst.DESCRIPTION.name()));
        task.setName(row.getString(FieldConst.NAME.name()));
        task.setStatus(StatusEnum.valueOf(row.getString(FieldConst.STATUS.name())));
        task.setCreatedData(FormatConvertUtil.convertSqlDateToDate(row.getDate(FieldConst.DATE_CREATE.name())));
        task.setStartDate(FormatConvertUtil.convertSqlDateToDate(row.getDate(FieldConst.DATE_START.name())));
        task.setEndDate(FormatConvertUtil.convertSqlDateToDate(row.getDate(FieldConst.DATE_END.name())));
        return task;
    }
}
