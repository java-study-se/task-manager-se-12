package com.morozov.tm.repository;

import com.morozov.tm.api.IProjectRepository;
import com.morozov.tm.entity.Project;
import com.morozov.tm.enumerated.FieldConst;
import com.morozov.tm.enumerated.StatusEnum;
import com.morozov.tm.exception.ConnectionLostException;
import com.morozov.tm.util.FormatConvertUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public final class ProjectRepository extends AbstractRepository implements IProjectRepository<Project> {

    public ProjectRepository(@Nullable Connection connection) {
        super(connection);
    }

    @NotNull
    @Override
    public List<Project> findAllByUserId(@NotNull final String userId) throws ConnectionLostException, SQLException {
        @NotNull final String query = "SELECT * FROM tm_morozov.app_project WHERE user_id = ?;";
        if (getConnection() == null) throw new ConnectionLostException();
        @NotNull final PreparedStatement statement = getConnection().prepareStatement(query);
        statement.setString(1, userId);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        @NotNull final List<Project> resultList = new ArrayList<>();
        while (resultSet.next()) resultList.add(fetch(resultSet));
        statement.close();
        return resultList;
    }

    @Nullable
    @Override
    public Project findOneByUserId(@NotNull final String userId, @NotNull final String id)
            throws ConnectionLostException, SQLException {
        Project resultProject = null;
        for (@NotNull final Project project : findAllByUserId(userId)) {
            if (project.getId().equals(id)) resultProject = project;
        }
        return resultProject;
    }

    @NotNull
    @Override
    public List<Project> findProjectByStringInNameOrDescription(@NotNull final String userId, @NotNull final String string)
            throws ConnectionLostException, SQLException {
        @NotNull final String query = "SELECT * FROM tm_morozov.app_project WHERE user_id = '" + userId + "' AND " +
                "(name LIKE '%" + string + "%' OR description LIKE '%" + string + "%');";
        if (getConnection() == null) throw new ConnectionLostException();
        @NotNull final PreparedStatement statement = getConnection().prepareStatement(query);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        @NotNull final List<Project> resultList = new ArrayList<>();
        while (resultSet.next()) resultList.add(fetch(resultSet));
        statement.close();
        return resultList;
    }

    @Override
    public void removeAllByUserId(@NotNull final String userId) throws ConnectionLostException, SQLException {
        @NotNull final String query = "DELETE FROM tm_morozov.app_project WHERE user_id = ?;";
        if (getConnection() == null) throw new ConnectionLostException();
        @NotNull final PreparedStatement statement = getConnection().prepareStatement(query);
        statement.setString(1, userId);
        statement.execute();
    }


    @Override
    public @NotNull List<Project> findAll() throws ConnectionLostException, SQLException {
        @NotNull final String query = "SELECT * FROM tm_morozov.app_project;";
        if (getConnection() == null) throw new ConnectionLostException();
        @NotNull final PreparedStatement statement = getConnection().prepareStatement(query);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        @NotNull final List<Project> resultList = new ArrayList<>();
        while (resultSet.next()) resultList.add(fetch(resultSet));
        statement.close();
        return resultList;
    }

    @Nullable
    @Override
    public Project findOne(@NotNull String id) throws SQLException, ConnectionLostException {
        @NotNull final String query = "SELECT * FROM tm_morozov.app_project WHERE id = ?;";
        if (getConnection() == null) throw new ConnectionLostException();
        @NotNull final PreparedStatement statement = getConnection().prepareStatement(query);
        statement.setString(1, id);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        @Nullable Project project = null;
        while (resultSet.next()) project = fetch(resultSet);
        return project;
    }

    @Override
    public void persist(@NotNull Project writeEntity) throws ConnectionLostException, SQLException {
        @NotNull final String query = "INSERT INTO tm_morozov.app_project (id, date_create, date_end, date_start, description," +
                " name, status, user_id) VALUES (?,?,?,?,?,?,?,?);";
        if (getConnection() == null) throw new ConnectionLostException();
        @NotNull final PreparedStatement statement = getConnection().prepareStatement(query);
        statement.setString(1, writeEntity.getId());
        statement.setDate(2, FormatConvertUtil.convertDateToSqlDate(writeEntity.getCreatedData()));
        statement.setDate(3, FormatConvertUtil.convertDateToSqlDate(writeEntity.getEndDate()));
        statement.setDate(4, FormatConvertUtil.convertDateToSqlDate(writeEntity.getStartDate()));
        statement.setString(5, writeEntity.getDescription());
        statement.setString(6, writeEntity.getName());
        statement.setString(7, writeEntity.getStatus().name());
        statement.setString(8, writeEntity.getUserId());
        statement.execute();
    }

    @Override
    public void merge(@NotNull Project updateEntity) throws ConnectionLostException, SQLException {
        if (findOne(updateEntity.getId()) == null) {
            persist(updateEntity);
        } else {
            @NotNull final String query = "UPDATE tm_morozov.app_project SET " +
                    "date_create = ? , date_end = ? , date_start = ? , description = ? , " +
                    "name = ?, status = ? , user_id = ? WHERE  id = ?;";
            if (getConnection() == null) throw new ConnectionLostException();
            @NotNull final PreparedStatement statement = getConnection().prepareStatement(query);
            statement.setDate(1, FormatConvertUtil.convertDateToSqlDate(updateEntity.getCreatedData()));
            statement.setDate(2, FormatConvertUtil.convertDateToSqlDate(updateEntity.getEndDate()));
            statement.setDate(3, FormatConvertUtil.convertDateToSqlDate(updateEntity.getStartDate()));
            statement.setString(4, updateEntity.getDescription());
            statement.setString(5, updateEntity.getName());
            statement.setString(6, updateEntity.getStatus().name());
            statement.setString(7, updateEntity.getUserId());
            statement.setString(8, updateEntity.getId());
            statement.execute();
        }
    }

    @Override
    public void remove(@NotNull String id) throws ConnectionLostException, SQLException {
        @NotNull final String query = "DELETE FROM tm_morozov.app_project WHERE id=?;";
        if (getConnection() == null) throw new ConnectionLostException();
        @NotNull final PreparedStatement statement = getConnection().prepareStatement(query);
        statement.setString(1, id);
        statement.execute();
    }

    @Override
    public void removeAll() throws ConnectionLostException, SQLException {
        @NotNull final String query = "DELETE FROM tm_morozov.app_project;";
        if (getConnection() == null) throw new ConnectionLostException();
        @NotNull final PreparedStatement statement = getConnection().prepareStatement(query);
        statement.execute();
    }

    @Override
    public void load(@NotNull List<Project> loadEntityList) throws ConnectionLostException, SQLException {
        for (@NotNull final Project project : loadEntityList) {
            merge(project);
        }
    }

    @Nullable
    private Project fetch(@Nullable final ResultSet row) throws SQLException {
        if (row == null) return null;
        @NotNull final Project project = new Project();
        project.setName(row.getString(FieldConst.NAME.name()));
        project.setId(row.getString(FieldConst.ID.name()));
        project.setUserId(row.getString(FieldConst.USER_ID.name()));
        project.setDescription(row.getString(FieldConst.DESCRIPTION.name()));
        project.setCreatedData(FormatConvertUtil.convertSqlDateToDate(row.getDate(FieldConst.DATE_CREATE.name())));
        project.setStartDate(FormatConvertUtil.convertSqlDateToDate(row.getDate(FieldConst.DATE_START.name())));
        project.setEndDate(FormatConvertUtil.convertSqlDateToDate(row.getDate(FieldConst.DATE_END.name())));
        project.setStatus(StatusEnum.valueOf(row.getString(FieldConst.STATUS.name())));
        return project;
    }
}
