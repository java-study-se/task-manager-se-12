package com.morozov.tm.repository;

import com.morozov.tm.api.ISessionRepository;
import com.morozov.tm.entity.Session;
import com.morozov.tm.enumerated.FieldConst;
import com.morozov.tm.enumerated.UserRoleEnum;
import com.morozov.tm.exception.AccessFirbidenException;
import com.morozov.tm.exception.ConnectionLostException;
import com.morozov.tm.util.FormatConvertUtil;
import com.morozov.tm.util.SessionUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class SessionRepository extends AbstractRepository implements ISessionRepository {
    public SessionRepository(@Nullable Connection connection) {
        super(connection);
    }

    @Override
    public @NotNull List<Session> findAll() throws ConnectionLostException, SQLException {
        @NotNull final String query = "SELECT * FROM tm_morozov.app_session;";
        if (getConnection() == null) throw new ConnectionLostException();
        @NotNull final PreparedStatement statement = getConnection().prepareStatement(query);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        @NotNull final List<Session> resultList = new ArrayList<>();
        while (resultSet.next()) resultList.add(fetch(resultSet));
        statement.close();
        return resultList;
    }

    @Nullable
    @Override
    public Session findOne(@NotNull String id) throws SQLException, ConnectionLostException {
        @NotNull final String query = "SELECT * FROM tm_morozov.app_session WHERE id = ?;";
        if (getConnection() == null) throw new ConnectionLostException();
        @NotNull final PreparedStatement statement = getConnection().prepareStatement(query);
        statement.setString(1, id);
        @NotNull final ResultSet resultSet = statement.executeQuery();
        //@Nullable Session session = fetch(resultSet);
        @Nullable Session session = new Session();
        while (resultSet.next()) session = fetch(resultSet);
        return session;
    }

    @Override
    public void persist(@NotNull Session writeEntity) throws ConnectionLostException, SQLException {
        @NotNull final String query = "INSERT INTO tm_morozov.app_session (id, signature, user_id, timestamp)" +
                " VALUES (?,?,?,?);";
        if (getConnection() == null) throw new ConnectionLostException();
        @NotNull final PreparedStatement statement = getConnection().prepareStatement(query);
        statement.setString(1, writeEntity.getId());
        statement.setString(2, writeEntity.getSignature());
        statement.setString(3, writeEntity.getUserId());
        statement.setDate(4, FormatConvertUtil.convertDateToSqlDate(writeEntity.getTimestamp()));
        statement.execute();
    }

    @Override
    public void merge(@NotNull Session updateEntity) throws ConnectionLostException, SQLException {
        if (findOne(updateEntity.getId()) == null) {
            persist(updateEntity);
        } else {
            @NotNull final String query = "UPDATE tm_morozov.app_session SET " +
                    "signature = ? , timestamp = ? , user_id = ? WHERE id = ?;";
            if (getConnection() == null) throw new ConnectionLostException();
            @NotNull final PreparedStatement statement = getConnection().prepareStatement(query);
            statement.setString(1, updateEntity.getSignature());
            statement.setDate(2, FormatConvertUtil.convertDateToSqlDate(updateEntity.getTimestamp()));
            statement.setString(3, updateEntity.getUserId());
            statement.execute();
        }
    }

    @Override
    public void remove(@NotNull String id) throws ConnectionLostException, SQLException {
        @NotNull final String query = "DELETE FROM tm_morozov.app_session WHERE id = ?;";
        if (getConnection() == null) throw new ConnectionLostException();
        @NotNull final PreparedStatement statement = getConnection().prepareStatement(query);
        statement.setString(1, id);
        statement.execute();
    }

    @Override
    public void removeAll() throws ConnectionLostException, SQLException {
        @NotNull final String query = "DELETE FROM tm_morozov.app_session;";
        if (getConnection() == null) throw new ConnectionLostException();
        @NotNull final PreparedStatement statement = getConnection().prepareStatement(query);
        statement.execute();
    }

    @Override
    public void load(@NotNull List<Session> loadEntityList) throws ConnectionLostException, SQLException {
        for (@NotNull final Session session : loadEntityList) {
            merge(session);
        }
    }

    @Nullable
    private Session fetch(@Nullable final ResultSet row) throws SQLException {
        if (row == null) return null;
        @NotNull final Session session = new Session();
        session.setId(row.getString(FieldConst.ID.name()));
        session.setSignature(row.getString(FieldConst.SIGNATURE.name()));
        session.setUserId(row.getString(FieldConst.USER_ID.name()));
        session.setTimestamp(FormatConvertUtil.convertSqlDateToDate(row.getDate(FieldConst.TIMESTAMP.name())));
        return session;
    }
}