package com.morozov.tm.repository;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;

import java.sql.Connection;
@NoArgsConstructor
@Getter
public abstract class AbstractRepository{

    @Nullable
    private Connection connection;

    public AbstractRepository(@Nullable final Connection connection) {
        this.connection = connection;
    }
}
