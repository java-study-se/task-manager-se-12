package com.morozov.tm.util;

import org.jetbrains.annotations.Nullable;

import java.sql.Date;

public class FormatConvertUtil {
    @Nullable
    public static Date convertDateToSqlDate(@Nullable java.util.Date date){
        Date sqlDate = null;
        if(date != null) sqlDate = new Date(date.getTime());
        return sqlDate;
    }
    public static java.util.Date convertSqlDateToDate(@Nullable Date sqlDate){
        java.util.Date date = null;
        if(sqlDate != null) date = new java.util.Date(sqlDate.getTime());
        return date;
    }
}
