package com.morozov.tm.util.comparator;


import com.morozov.tm.entity.AbstractWorkEntity;
import org.jetbrains.annotations.NotNull;

import java.util.Comparator;

public class DataCreateComparator implements Comparator<AbstractWorkEntity> {
    @Override
    public int compare(@NotNull final AbstractWorkEntity o1, @NotNull final AbstractWorkEntity o2) {
        return o1.getCreatedData().compareTo(o2.getCreatedData());
    }
}
