package com.morozov.tm.util;

import com.morozov.tm.exception.StringEmptyException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

public class DbConnectionUtil {
    @Nullable
    public static Connection getConnectin() {
        @NotNull final Properties dbConnectionProperties = new Properties();
        @Nullable FileInputStream fis;
        @Nullable Connection connection = null;
        try {
            fis = new FileInputStream("tm-server/src/main/resources/dbconnection.properties");
            dbConnectionProperties.load(fis);
            @Nullable String urlConnection = dbConnectionProperties.getProperty("url");
            @Nullable String userConnection = dbConnectionProperties.getProperty("login");
            @Nullable String passwordConnection = dbConnectionProperties.getProperty("password");
            if (urlConnection == null) throw new StringEmptyException();
            connection = DriverManager.getConnection(urlConnection, userConnection, passwordConnection);
            if (connection != null) System.out.println("OK");
        } catch (IOException | SQLException | StringEmptyException e) {
            e.printStackTrace();
        }
        return connection;
    }
}
