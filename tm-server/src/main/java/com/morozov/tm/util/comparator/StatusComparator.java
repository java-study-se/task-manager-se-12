package com.morozov.tm.util.comparator;

import com.morozov.tm.entity.AbstractWorkEntity;
import org.jetbrains.annotations.NotNull;

import java.util.Comparator;

public class StatusComparator implements Comparator<AbstractWorkEntity> {
    @Override
    public int compare(@NotNull final AbstractWorkEntity o1, @NotNull final AbstractWorkEntity o2) {
        return o1.getStatus().getIndexForCompare().compareTo(o2.getStatus().getIndexForCompare());
    }
}
