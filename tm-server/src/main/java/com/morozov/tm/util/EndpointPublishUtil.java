package com.morozov.tm.util;

import com.morozov.tm.endpoint.*;
import com.morozov.tm.service.Bootstrap;
import org.jetbrains.annotations.NotNull;

import javax.xml.ws.Endpoint;

public class EndpointPublishUtil {
    private static final String URL = "http://localhost:8080/";

    public static void publish(Bootstrap bootstrap) {
        ConsoleHelperUtil.writeString("Публикация Endpoint:");
        @NotNull final TaskEndpoint taskEndpoint = new TaskEndpoint(bootstrap);
        @NotNull final String linkUrlTaskEndpoint = URL + taskEndpoint.getClass().getSimpleName() + "?wsdl";
        Endpoint.publish(linkUrlTaskEndpoint, taskEndpoint);
        ConsoleHelperUtil.writeString("Опубликован " + taskEndpoint.getClass().getSimpleName() + ": " + linkUrlTaskEndpoint);
        @NotNull final ProjectEndpoint projectEndpoint = new ProjectEndpoint(bootstrap);
        @NotNull final String linkUrlProjectEndpoint = URL + projectEndpoint.getClass().getSimpleName() + "?wsdl";
        Endpoint.publish(linkUrlProjectEndpoint, projectEndpoint);
        ConsoleHelperUtil.writeString("Опубликован " + projectEndpoint.getClass().getSimpleName() + ": " + linkUrlProjectEndpoint);
        @NotNull final UserEndpoint userEndpoint = new UserEndpoint(bootstrap);
        @NotNull final String linkUrlUserEndPoint = URL + userEndpoint.getClass().getSimpleName() + "?wsdl";
        Endpoint.publish(linkUrlUserEndPoint, userEndpoint);
        ConsoleHelperUtil.writeString("Опубликован " + userEndpoint.getClass().getSimpleName() + ": " + linkUrlUserEndPoint);
        @NotNull final DomainEndpoint domainEndpoint = new DomainEndpoint(bootstrap);
        @NotNull final String linkUrlDomainEndpoint = URL + domainEndpoint.getClass().getSimpleName() + "?wsdl";
        Endpoint.publish(linkUrlDomainEndpoint, domainEndpoint);
        ConsoleHelperUtil.writeString("Опубликован " + domainEndpoint.getClass().getSimpleName() + ": " + linkUrlDomainEndpoint);
        @NotNull final SessionEndpoint sessionEndpoint = new SessionEndpoint(bootstrap);
        @NotNull final String linkUrlSessionEndpoint = URL + sessionEndpoint.getClass().getSimpleName() + "?wsdl";
        Endpoint.publish(linkUrlSessionEndpoint, sessionEndpoint);
        ConsoleHelperUtil.writeString("Опубликован " + sessionEndpoint.getClass().getSimpleName() + ": " + linkUrlSessionEndpoint);
    }
}
