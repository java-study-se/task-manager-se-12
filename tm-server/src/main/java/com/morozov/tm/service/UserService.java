package com.morozov.tm.service;

import com.morozov.tm.api.IUserRepository;
import com.morozov.tm.api.IUserService;
import com.morozov.tm.entity.User;
import com.morozov.tm.enumerated.UserRoleEnum;
import com.morozov.tm.exception.ConnectionLostException;
import com.morozov.tm.exception.StringEmptyException;
import com.morozov.tm.exception.UserExistException;
import com.morozov.tm.exception.UserNotFoundException;
import com.morozov.tm.util.DataValidationUtil;
import com.morozov.tm.util.MD5HashUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public final class UserService implements IUserService {

    final private IUserRepository userRepository;

    public UserService(@NotNull final IUserRepository UserRepository) {
        this.userRepository = UserRepository;
    }

    @NotNull
    @Override
    public User loginUser(@NotNull final String login, @NotNull final String password)
            throws UserNotFoundException, StringEmptyException, ConnectionLostException {
        DataValidationUtil.checkEmptyString(login, password);
        @Nullable User user = null;
        try {
            user = (User) userRepository.findOneByLogin(login);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        if (user == null) throw new UserNotFoundException();
        final String typePasswordHash = MD5HashUtil.getHash(password);
        if (typePasswordHash == null) throw new UserNotFoundException();
        if (!typePasswordHash.equals(user.getPasswordHash())) throw new UserNotFoundException();
        return user;
    }

    @NotNull
    @Override
    public String registryUser(@NotNull final String login, @NotNull final String password)
            throws UserExistException, StringEmptyException, ConnectionLostException {
        DataValidationUtil.checkEmptyString(login, password);
        User oneByLogin = null;
        try {
            oneByLogin = (User) userRepository.findOneByLogin(login);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        if (oneByLogin != null) throw new UserExistException();
        @NotNull final User user = new User();
        user.setLogin(login);
        user.setRole(UserRoleEnum.USER);
        final String hashPassword = MD5HashUtil.getHash(password);
        if (hashPassword == null) throw new StringEmptyException();
        user.setPasswordHash(hashPassword);
        @NotNull final String userId = user.getId();
        try {
            userRepository.persist(user);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return userId;
    }


    @Override
    public void updateUserPassword(@NotNull final String id, @NotNull final String newPassword) throws StringEmptyException,
            UserNotFoundException, ConnectionLostException {
        @Nullable User user = null;
        try {
            user = (User) userRepository.findOne(id);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        if (user != null && !user.getId().equals(id)) throw new UserNotFoundException();
        DataValidationUtil.checkEmptyString(newPassword);
        if (user == null) throw new UserNotFoundException();
        String hashNewPassword = MD5HashUtil.getHash(newPassword);
        if (hashNewPassword != null) {
            user.setPasswordHash(hashNewPassword);
        }
        try {
            userRepository.merge(user);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void updateUserProfile(@NotNull final String id, @NotNull final String newUserName, @NotNull final String newUserPassword)
            throws StringEmptyException, UserExistException, UserNotFoundException, ConnectionLostException {
        try {
            @Nullable User user = (User) userRepository.findOne(id);
            if (user != null && !user.getId().equals(id)) throw new UserNotFoundException();
            DataValidationUtil.checkEmptyString(newUserName, newUserPassword);
            if (userRepository.findOneByLogin(newUserName) != null) throw new UserExistException();
            if (user == null) throw new UserNotFoundException();
            user.setLogin(newUserName);
            String hashNewUserPassword = MD5HashUtil.getHash(newUserPassword);
            if (hashNewUserPassword != null) {
                user.setPasswordHash(hashNewUserPassword);
            }
            userRepository.merge(user);
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public void loadUserList(@Nullable final List<User> userList) throws ConnectionLostException {
        if (userList != null) {
            try {
                userRepository.load(userList);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public @NotNull List<User> findAllUser() throws ConnectionLostException {
        @NotNull List<User> resultList = new ArrayList<>();
        try {
            resultList = userRepository.findAll();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return resultList;
    }

    @NotNull
    @Override
    public User findOneById(@NotNull String id) throws UserNotFoundException, ConnectionLostException {
        @Nullable User user = null;
        try {
            user = (User) userRepository.findOne(id);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        if (user == null) throw new UserNotFoundException();
        if (!user.getId().equals(id)) throw new UserNotFoundException();
        return user;
    }

    @Override
    public void fillUser(User user) {
        try {
            userRepository.persist(user);
        } catch (ConnectionLostException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
