package com.morozov.tm.service;

import com.morozov.tm.api.IProjectRepository;
import com.morozov.tm.entity.Project;
import com.morozov.tm.enumerated.StatusEnum;
import com.morozov.tm.exception.ConnectionLostException;
import com.morozov.tm.exception.ProjectNotFoundException;
import com.morozov.tm.exception.RepositoryEmptyException;
import com.morozov.tm.exception.StringEmptyException;
import com.morozov.tm.util.ConsoleHelperUtil;
import com.morozov.tm.util.DataValidationUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public final class ProjectService implements com.morozov.tm.api.IProjectService {

    final private IProjectRepository projectRepository;

    public ProjectService(@NotNull IProjectRepository ProjectRepository) {
        this.projectRepository = ProjectRepository;
    }

    @Override
    @NotNull
    public final List<Project> findAllProject() throws ConnectionLostException {
        @NotNull List<Project> projectList = new ArrayList<>();
        try {
            projectList = projectRepository.findAll();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return projectList;
    }

    @Override
    @NotNull
    public final List<Project> getAllProjectByUserId(@NotNull String userId) throws RepositoryEmptyException, ConnectionLostException {
        @NotNull List<Project> projectListByUserId = new ArrayList<>();
        try {
            projectListByUserId = projectRepository.findAllByUserId(userId);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        DataValidationUtil.checkEmptyRepository(projectListByUserId);
        return projectListByUserId;
    }
    @NotNull
    @Override
    public final Project addProject(@NotNull final String userId, @NotNull final String projectName)
            throws StringEmptyException, ConnectionLostException {
        DataValidationUtil.checkEmptyString(projectName);
        @NotNull final Project project = new Project();
        project.setName(projectName);
        project.setUserId(userId);
        try {
            projectRepository.persist(project);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return project;
    }

    @Override
    public final boolean removeProjectById(@NotNull final String userId, @NotNull String id) throws StringEmptyException, ConnectionLostException {
        boolean isDeleted = false;
        DataValidationUtil.checkEmptyString(id);
        try {
            if (projectRepository.findOneByUserId(userId, id) != null) {
                projectRepository.remove(id);
                isDeleted = true;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return isDeleted;
    }

    @Override
    public final void updateProject(@NotNull final String userId, @NotNull final String id, @NotNull final String projectName,
            @NotNull final String projectDescription, @NotNull final String dataStart, @NotNull final String dataEnd)
            throws RepositoryEmptyException, StringEmptyException, ParseException, ProjectNotFoundException, ConnectionLostException {
        try {
            DataValidationUtil.checkEmptyRepository(projectRepository.findAllByUserId(userId));
            DataValidationUtil.checkEmptyString(id, projectName, projectDescription);
            final Project project = (Project) projectRepository.findOne(id);
            if(project == null) throw new ProjectNotFoundException();
            project.setId(id);
            project.setName(projectName);
            project.setDescription(projectDescription);
            final Date updatedStartDate = ConsoleHelperUtil.formattedData(dataStart);
            if (updatedStartDate != null) {
                project.setStatus(StatusEnum.PROGRESS);
            }
            project.setStartDate(updatedStartDate);
            final Date updatedEndDate = ConsoleHelperUtil.formattedData(dataEnd);
            if (updatedEndDate != null) {
                project.setStatus(StatusEnum.READY);
            }
            project.setEndDate(updatedEndDate);
            project.setUserId(userId);
            projectRepository.merge(project);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    @NotNull
    public final List<Project> findProjectByStringInNameOrDescription(@NotNull final String userId,@NotNull final String string)
            throws  ConnectionLostException {
        @NotNull List<Project> projectListByUserId = new ArrayList<>();
        try {
            if (string.isEmpty()) return projectRepository.findAllByUserId(userId);
            projectListByUserId = projectRepository.findProjectByStringInNameOrDescription(userId, string);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return projectListByUserId;
    }

    @Override
    public void removeAllByUserId(@NotNull final String userId) throws ConnectionLostException {
        try {
            projectRepository.removeAllByUserId(userId);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public final void clearProjectList() throws ConnectionLostException {
        try {
            projectRepository.removeAll();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void loadProjectList(@Nullable List<Project> loadProjectList) throws ConnectionLostException {
        if (loadProjectList != null) {
            try {
                projectRepository.load(loadProjectList);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

    }

}
