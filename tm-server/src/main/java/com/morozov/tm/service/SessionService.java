package com.morozov.tm.service;

import com.morozov.tm.api.ISessionRepository;
import com.morozov.tm.api.ISessionService;
import com.morozov.tm.entity.Session;
import com.morozov.tm.entity.User;
import com.morozov.tm.exception.AccessFirbidenException;
import com.morozov.tm.exception.ConnectionLostException;
import com.morozov.tm.util.SessionUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.sql.SQLException;
import java.util.Date;

public class SessionService implements ISessionService {
    @NotNull
    private ISessionRepository sessionRepository;

    public SessionService(@NotNull ISessionRepository sessionRepository) {
        this.sessionRepository = sessionRepository;
    }
    @Override
    public Session getNewSession(User user) throws ConnectionLostException {
        @NotNull final Session session = new Session();
        session.setUserId(user.getId());
        session.setUserRoleEnum(user.getRole());
        session.setTimestamp(new Date());
        Session sessionWithSign = SessionUtil.sign(session);
        try {
            sessionRepository.persist(session);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return sessionWithSign;
    }

    @NotNull
    @Override
    public Session validate(@Nullable final Session session) throws AccessFirbidenException, CloneNotSupportedException, ConnectionLostException {
        if (session == null) throw new AccessFirbidenException();
        if (session.getSignature() == null || session.getSignature().isEmpty()) throw new AccessFirbidenException();
        if (session.getUserId() == null || session.getUserId().isEmpty()) throw new AccessFirbidenException();
        if (session.getTimestamp() == null || !SessionUtil.isSessionTimeLive(session.getTimestamp())) throw new AccessFirbidenException();
        @NotNull final Session temp = session.clone();
        if (temp == null) throw new AccessFirbidenException();
        @NotNull final String signatureOrigin = session.getSignature();
        @NotNull final String signatureTemp = SessionUtil.sign(temp).getSignature();
        final boolean check = signatureOrigin.equals(signatureTemp);
        if (!check) throw new AccessFirbidenException();
        try {
            if (sessionRepository.findOne(session.getId()) == null) throw new AccessFirbidenException();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return session;
    }

    @Override
    public void closeSession(@NotNull final Session session) throws ConnectionLostException {
        try {
            sessionRepository.remove(session.getId());
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

}
