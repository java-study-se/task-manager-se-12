package com.morozov.tm.service;

import com.morozov.tm.api.ITaskRepository;
import com.morozov.tm.api.ITaskService;
import com.morozov.tm.entity.Task;
import com.morozov.tm.enumerated.StatusEnum;
import com.morozov.tm.exception.ConnectionLostException;
import com.morozov.tm.exception.RepositoryEmptyException;
import com.morozov.tm.exception.StringEmptyException;
import com.morozov.tm.exception.TaskNotFoundException;
import com.morozov.tm.util.ConsoleHelperUtil;
import com.morozov.tm.util.DataValidationUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


public final class TaskService implements ITaskService {

    final private ITaskRepository taskRepository;

    public TaskService(@NotNull ITaskRepository TaskRepository) {
        this.taskRepository = TaskRepository;
    }

    @Override
    public @NotNull List<Task> findAllTask() throws ConnectionLostException {
        @NotNull  List<Task> taskList = new ArrayList<>();
        try {
            taskList = taskRepository.findAll();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return taskList;
    }

    @Override
    @NotNull
    public List<Task> getAllTaskByUserId(@NotNull final String userId)
            throws RepositoryEmptyException, ConnectionLostException {
        @NotNull  List<Task> taskList = new ArrayList<>();
        try {
            taskList = taskRepository.findAllByUserId(userId);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        DataValidationUtil.checkEmptyRepository(taskList);
        return taskList;
    }

    @Override
    @NotNull
    public Task addTask(@NotNull final String userId, @NotNull final String taskName, @NotNull final String projectId)
            throws StringEmptyException, ConnectionLostException {
        DataValidationUtil.checkEmptyString(taskName, projectId);
        @NotNull final Task task = new Task();
        task.setName(taskName);
        task.setIdProject(projectId);
        task.setUserId(userId);
        try {
            taskRepository.persist(task);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return task;
    }

    @Override
    public boolean removeTaskById(@NotNull final String userId, @NotNull final String id)
            throws StringEmptyException, ConnectionLostException {
        boolean isDeleted = false;
        DataValidationUtil.checkEmptyString(id);
        try {
            if (taskRepository.findOneByUserId(userId, id) != null) {
                taskRepository.remove(id);
                isDeleted = true;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return isDeleted;
    }

    @Override
    public void updateTask(
            @NotNull final String userId, @NotNull final String id, @NotNull final String name,
            @NotNull final String description, @NotNull final String dataStart, @NotNull final String dataEnd,
            @NotNull final String projectId)
            throws RepositoryEmptyException, StringEmptyException, ParseException, TaskNotFoundException, ConnectionLostException {
        try {
            DataValidationUtil.checkEmptyRepository(taskRepository.findAll());
            DataValidationUtil.checkEmptyString(id, name, description, projectId);
            final Task task = (Task) taskRepository.findOne(id);
            if (task == null) throw new TaskNotFoundException();
            task.setId(id);
            task.setName(name);
            task.setDescription(description);
            final Date updatedStartDate = ConsoleHelperUtil.formattedData(dataStart);
            if (updatedStartDate != null) {
                task.setStatus(StatusEnum.PROGRESS);
            }
            task.setStartDate(updatedStartDate);
            final Date updatedEndDate = ConsoleHelperUtil.formattedData(dataEnd);
            if (updatedEndDate != null) {
                task.setStatus(StatusEnum.READY);
            }
            task.setEndDate(updatedEndDate);
            task.setIdProject(projectId);
            task.setUserId(userId);
            taskRepository.merge(task);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    @NotNull
    public List<Task> getAllTaskByProjectId(@NotNull final String userId, @NotNull final String projectId)
            throws StringEmptyException, RepositoryEmptyException, ConnectionLostException {
        DataValidationUtil.checkEmptyString(projectId);
        @NotNull List<Task> resultTaskList = new ArrayList<>();
        try {
            resultTaskList = taskRepository.findAllByProjectIdUserId(userId, projectId);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        DataValidationUtil.checkEmptyRepository(resultTaskList);
        return resultTaskList;
    }

    @Override
    public void removeAllTaskByProjectId(@NotNull final String userId, @NotNull final String projectId)
            throws ConnectionLostException {
        try {
            taskRepository.deleteAllTaskByProjectId(userId, projectId);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void clearTaskList() throws ConnectionLostException {
        try {
            taskRepository.removeAll();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    @NotNull
    public List<Task> findTaskByStringInNameOrDescription(@NotNull final String userId, @NotNull final String string)
            throws ConnectionLostException {
        @NotNull  List<Task> taskListByProjectId = new ArrayList<>();
        try {
            if (string.isEmpty()) return taskRepository.findAllByUserId(userId);
            taskListByProjectId = taskRepository.findTaskByStringInNameOrDescription(userId, string);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return taskListByProjectId;
    }

    @Override
    public void removeAllTaskByUserId(@NotNull final String userId) throws ConnectionLostException {
        try {
            taskRepository.removeAllByUserId(userId);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void loadTaskList(@Nullable final List<Task> loadList) throws ConnectionLostException {
        try {
            if (loadList != null) taskRepository.load(loadList);
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }
}
