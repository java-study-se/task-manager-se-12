package com.morozov.tm.api;


import com.morozov.tm.entity.AbstractWorkEntity;
import com.morozov.tm.exception.ConnectionLostException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.sql.SQLException;
import java.util.List;

public interface IProjectRepository<T extends AbstractWorkEntity> extends IRepository<T> {
    @NotNull
    List<T> findAllByUserId(@NotNull final String userId) throws ConnectionLostException, SQLException;

    @Nullable
    T findOneByUserId(@NotNull final String userId, @NotNull final String id) throws ConnectionLostException, SQLException;
    @NotNull
    List<T> findProjectByStringInNameOrDescription(@NotNull final String userId, @NotNull final String string) throws ConnectionLostException, SQLException;

    void removeAllByUserId(@NotNull final String userId) throws ConnectionLostException, SQLException;
}
