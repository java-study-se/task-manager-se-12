package com.morozov.tm.api;

import com.morozov.tm.entity.AbstractEntity;
import com.morozov.tm.exception.ConnectionLostException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.sql.SQLException;

public interface IUserRepository<T extends AbstractEntity> extends IRepository<T> {
    @Nullable
    T findOneByLogin(@NotNull final String login) throws ConnectionLostException, SQLException;
}

