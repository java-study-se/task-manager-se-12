package com.morozov.tm.api;

import com.morozov.tm.entity.Session;
import com.morozov.tm.exception.AccessFirbidenException;
import org.jetbrains.annotations.NotNull;

public interface ISessionRepository extends IRepository<Session>{

}
