package com.morozov.tm.api;

import com.morozov.tm.entity.AbstractWorkEntity;
import com.morozov.tm.exception.ConnectionLostException;
import org.jetbrains.annotations.NotNull;

import java.sql.SQLException;
import java.util.List;

public interface ITaskRepository<T extends AbstractWorkEntity> extends IRepository<T> {

    List<T> findAllByProjectIdUserId(@NotNull final String userId, @NotNull final String projectId) throws ConnectionLostException, SQLException;

    List<T> findAllByUserId(@NotNull final String userId) throws ConnectionLostException, SQLException;

    T findOneByUserId(@NotNull final String userId, @NotNull final String id) throws ConnectionLostException, SQLException;

    List<T> getAllTaskByProjectId(@NotNull final String projectId) throws ConnectionLostException, SQLException;

    void deleteAllTaskByProjectId(@NotNull final String userId, @NotNull final String projectId) throws ConnectionLostException, SQLException;

    List<T> findTaskByStringInNameOrDescription(@NotNull final String userId, @NotNull final String string) throws ConnectionLostException, SQLException;

    void removeAllByUserId(@NotNull final String userId) throws ConnectionLostException, SQLException;
}
