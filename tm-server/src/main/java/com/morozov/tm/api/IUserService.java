package com.morozov.tm.api;

import com.morozov.tm.entity.User;
import com.morozov.tm.exception.ConnectionLostException;
import com.morozov.tm.exception.StringEmptyException;
import com.morozov.tm.exception.UserExistException;
import com.morozov.tm.exception.UserNotFoundException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;


import java.util.List;

public interface IUserService {
    @NotNull
    User loginUser(@NotNull String login, @NotNull String password) throws UserNotFoundException, StringEmptyException, ConnectionLostException;

    @NotNull
    String registryUser(@NotNull String login, @NotNull String password) throws UserExistException, StringEmptyException, ConnectionLostException;

    void updateUserPassword(@NotNull String id, @NotNull String newPassword) throws StringEmptyException, UserNotFoundException, ConnectionLostException;

    void updateUserProfile(@NotNull String id, @NotNull String newUserName, @NotNull String newUserPassword)
            throws StringEmptyException, UserExistException, UserNotFoundException, ConnectionLostException;

    void loadUserList(@Nullable final List<User> userList) throws ConnectionLostException;

    @NotNull
    List<User> findAllUser() throws ConnectionLostException;

    @NotNull
    User findOneById(@NotNull final String id) throws UserNotFoundException, ConnectionLostException;

    void fillUser(User user);
}
