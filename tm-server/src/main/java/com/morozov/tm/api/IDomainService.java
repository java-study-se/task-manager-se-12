package com.morozov.tm.api;

import com.morozov.tm.entity.Domain;
import com.morozov.tm.exception.ConnectionLostException;
import org.jetbrains.annotations.NotNull;

import java.sql.SQLException;

public interface IDomainService {
    void load(@NotNull Domain domain) throws ConnectionLostException;
    void export(@NotNull Domain domain) throws ConnectionLostException;
}
