package com.morozov.tm.api;


import com.morozov.tm.entity.User;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.sql.Connection;
import java.util.List;

public interface IServiceLocator {

    @NotNull
    IProjectService getProjectService();

    @NotNull
    ITaskService getTaskService();

    @NotNull
    IUserService getUserService();

    @NotNull
    IDomainService getDomainService();

    @NotNull
    ISessionService getSessionService();

    @Nullable
    Connection getConnection();
}
